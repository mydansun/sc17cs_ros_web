from server import Server


class Provider:
    def __init__(self, server):
        assert isinstance(server, Server)
        self.__server = server

    def _send_str(self, data):
        self.__server.send_str(data)
