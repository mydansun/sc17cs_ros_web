#! /usr/bin/env python
import importlib
import json
import os

import rospy

from server import Server


def to_camel_case(snake_str):
    components = snake_str.split('_')
    # We capitalize the first letter of each component except the first one
    # with the 'title' method and join them together.
    return components[0] + ''.join(x.title() for x in components[1:])


def shutdown():
    rospy.logwarn("Shutdown...")
    # noinspection PyProtectedMember
    os._exit(0)


def main():
    rospy.on_shutdown(shutdown)
    rospy.init_node('sc17cs_ros_web')
    config_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'config.json')
    if not os.path.exists(config_path):
        rospy.logerr("%s dose not exist" % config_path)
        shutdown()

    with open(config_path) as f:
        config_str = f.read().encode('utf-8')
    provider_names = json.loads(config_str)['providers']
    providers = []
    for provider_name in provider_names:
        class_name = to_camel_case(str(provider_name)).title()
        providers.append(getattr(importlib.import_module("providers." + provider_name), class_name))
    server = Server('127.0.0.1', 12011, providers, shutdown)
    rospy.spin()


if __name__ == '__main__':
    main()
