const net = require('net');
const ws = require('socket.io')(12012, {});

ws.on('connection', (socket) => {
    socket.join('ros');
});

const sock = net.connect(12011, '127.0.0.1', function () {
    console.log('server connected');
    sock.setEncoding('utf8');
    sock.write('Hello Echo Server\n');
});

let str_data = "";

sock.on('data', function (data) {
    data = data.toString();
    str_data = str_data + data;
    if (data.substr(data.length - 1) === "\n") {
        str_data = str_data.substr(0, str_data.length - 1);
        // console.log('got data from server - ', str_data);
        ws.to('ros').emit('msg', str_data);
        str_data = "";
    }
});

sock.on('end', function () {
    console.log('client disconnected');
});

sock.on('error', function (err) {
    console.log('socket error - ', err);
});