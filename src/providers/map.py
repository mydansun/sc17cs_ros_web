import base64
import numpy as np
import threading
import time
from numpy.core.multiarray import ndarray

import cv2
import rospy
import tf
from geometry_msgs.msg import Point
from nav_msgs.msg import OccupancyGrid
from sensor_msgs.msg import Image

from provider import Provider

ROBOT_COLOR = (34, 102, 0)
ROBOT_RADIUS = 0.27


class Map(Provider):
    def __init__(self, server):
        Provider.__init__(self, server)
        self.__map_width = 0
        self.__map_height = 0
        self.__map_resolution = 0.0
        self.__map_origin = None  # type: Point
        self.__map_rgb = None  # type: ndarray
        self.__map_publisher = rospy.Publisher('sc17cs_ros_web/map', Image, queue_size=5)

        self.__tf_listener = None  # type: tf.TransformListener
        self.__robot_pose = (0.0, 0.0, 0)
        self.__robot_pixel = (0, 0)
        self.__robot_radius_pixels = 0

        rospy.Subscriber("/map", OccupancyGrid, self.__make_map_rgb)
        self.__tf_listener = tf.TransformListener()
        rospy.loginfo("Ready to start tf listener...")
        time.sleep(3)

        env_thread = threading.Thread(target=self.__publish_env)
        env_thread.daemon = True
        env_thread.start()

    @staticmethod
    def euler_from_quaternion(quaternion):
        # noinspection PyUnresolvedReferences
        return tf.transformations.euler_from_quaternion(quaternion)  # type: list

    def pixel_to_coordinate(self, col, row=None):
        if hasattr(col, '__len__') and len(col) >= 2 and row is None:
            row = col[1]
            col = col[0]
        result = [round(col * self.__map_resolution + self.__map_origin.x, 2),
                  round((self.__map_height - row) * self.__map_resolution + self.__map_origin.y, 2)]
        return tuple(result)

    def coordinate_to_pixel(self, x, y=None):
        if hasattr(x, '__len__') and len(x) >= 2 and y is None:
            y = x[1]
            x = x[0]
        return int(round((x - self.__map_origin.x) / self.__map_resolution)), self.__map_height - int(
            round(((y - self.__map_origin.y) / self.__map_resolution)))

    def __publish_env(self):
        rate = rospy.Rate(5)
        while not rospy.is_shutdown() and self.__map_rgb is not None:
            try:
                (trans, quaternion) = self.__tf_listener.lookupTransform('/map', '/base_link', rospy.Time(0))
                x = round(trans[0], 2)
                y = round(trans[1], 2)
                euler = self.euler_from_quaternion(quaternion)
                yaw = round(euler[2], 2)
                self.__robot_pose = (x, y, yaw)
                self.__robot_pixel = self.coordinate_to_pixel(self.__robot_pose)
                output_image = self.__map_rgb.copy()

                cv2.circle(output_image, self.__robot_pixel, self.__robot_radius_pixels, ROBOT_COLOR, -1)
                _, image_buffer = cv2.imencode('.jpg', output_image)
                image_str = base64.b64encode(image_buffer).decode()
                self._send_str(image_str)
            except rospy.ROSException as e:
                rospy.logerr(e.message)
            rate.sleep()

    def __make_map_rgb(self, map_message):
        assert isinstance(map_message, OccupancyGrid)
        self.__map_width = map_message.info.width
        self.__map_height = map_message.info.height
        self.__map_resolution = map_message.info.resolution
        self.__map_origin = map_message.info.origin.position

        map_data = np.reshape(map_message.data, (self.__map_height, self.__map_width))
        map_rgb = np.zeros((self.__map_height, self.__map_width, 3), np.uint8)
        map_rgb.fill(205)
        for row in xrange(self.__map_height):
            for col in xrange(self.__map_width):
                probability = map_data[row, col]
                if probability == -1:
                    continue
                if probability > 0:
                    color = 0
                else:
                    color = 255
                # color = (1 - probability) * 255
                map_rgb[row, col] = (color, color, color)
        self.__map_rgb = cv2.flip(map_rgb, 0)
        self.__robot_radius_pixels = int(round(ROBOT_RADIUS / self.__map_resolution))
