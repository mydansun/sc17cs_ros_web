import socket
import threading

import rospy


class Server:
    def __init__(self, ip, port, providers, shutdown):
        assert callable(shutdown)
        assert isinstance(providers, list)
        self.shutdown = shutdown
        self.port = port
        self.ip = ip
        self.__socket_server = None  # type: socket.socket
        self.__conn = None  # type: socket.socket
        self.__providers = []

        socket_thread = threading.Thread(target=self.__start_socket_server)
        socket_thread.daemon = True
        socket_thread.start()

        for provider in providers:
            from provider import Provider
            assert issubclass(provider, Provider)
            self.__providers.append(provider(self))

    def __start_socket_server(self):
        self.__socket_server = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.__socket_server.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        try:
            self.__socket_server.bind((self.ip, self.port))
        except Exception as e:
            rospy.logerr(e)
            self.shutdown()

        rospy.loginfo("IP address: %s, Port: %s, listening...", self.ip, self.port)
        self.__socket_server.listen(5)
        while True:
            try:
                self.__conn, addr = self.__socket_server.accept()
            except socket.timeout:
                continue
            rospy.loginfo("%s joined the socket server.", addr)
            while self.__conn is not None:
                str_data = ""
                while True:
                    try:
                        byte_data = self.__conn.recv(1024)
                    except socket.timeout:
                        continue
                    if len(byte_data) == 0:
                        rospy.loginfo("%s leaved the socket server.", addr)
                        self.__conn.close()
                        self.__conn = None
                        break
                    str_data = str_data + byte_data.decode()
                    if str_data[-1] == "\n":
                        str_data = str_data[:-1]
                        break
                if len(str_data):
                    rospy.loginfo("Receive: %s", str_data)

    def send_str(self, str_data):
        if self.__conn is None:
            return
        if isinstance(str_data, unicode):
            str_data = str_data.encode('utf-8')
        assert isinstance(str_data, str)
        try:
            self.__conn.sendall(str_data + "\n")
        except socket.error as e:
            self.__conn.close()
            rospy.logerr(e)
            self.__conn = None
