# ROS Web Interface

Author: Chengke Sun

## Project Structure
[![B1LOJ0.png](https://s1.ax1x.com/2020/10/28/B1LOJ0.png)](https://imgchr.com/i/B1LOJ0)

## Dependencies

- [Node.js](https://nodejs.org/en/)

## Demo

1. Clone this repo as a catkin package in your workspace.
1. Go to the `src` directory.
1. Execute `cp config.example.json config.json`
1. Run a normal TIAGO navigation ROS node (a grid map is needed).
1. Execute `rosrun sc17cs_row_web main.py`
1. Go to the `ws_server` directory.
1. Execute `node main.js`
1. Open `test.html` in your browser.

## How to Development

1. Take a look at `src/providers/map.py` and you will get help.
1. You need to customize the content transmitted by the provider in the websocket, in order to transmit binary content you can use base64 encoding.
1. If you write a new provider, do not forget to register it on `src/config.json`
1. Till now, I haven't written an example of how to listen uploaded data from webpages and perform commands of ROS. However, all this should be implemented in providers. If I have time in the future, I will finish this feature.